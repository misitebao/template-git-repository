<p align="center">
  <img src="https://cdn.jsdelivr.net/gh/misitebao/CDN@master/gravatar_tigateam.png" width="40%" /><br/>
</p>
<p align="center">
Шаблон репозитория Git с открытым исходным кодом
</p>

<span id="nav-1"></span>

## 🌏 Интернационализация

[English](README.md) | [简体中文](README.zh-Hans.md) | [русский](README.ru.md) | [français](README.fr.md)

<span id="nav-2"></span>

## 📚 СОДЕРЖАНИЕ

- [Интернационализация](#nav-1)
- [СОДЕРЖАНИЕ](#nav-2)
- [Введение](#nav-3)
  - [Официальный веб-сайт](#nav-3-1)
- [Графическая демонстрация](#nav-4)
- [Функции](#nav-5)
- [Архитектура](#nav-6)
- [Начиная](#nav-7)
- [Авторы](#nav-8)
- [Авторы](#nav-9)
  - [Обмен сообществом](#nav-9-1)
- [Часть пользователей](#nav-10)
- [Release History](CHANGE.md)
- [Жертвователи](#nav-11)
- [Жертвователи](#nav-12)
- [Спасибо](#nav-13)
- [Лицензия](#nav-14)

<span id="nav-3"></span>

## ℹ️ Введение

Этот проект представляет собой образец шаблона хранилища Github, основное содержание - образец шаблона README.

<span id="nav-3-1"></span>

### 🔔 Официальный веб-сайт

<span id="nav-4"></span>

## 🌅 Графическая демонстрация

![](https://cdn.jsdelivr.net/gh/misitebao/CDN@main/md/20210727130417.png)

<span id="nav-5"></span>

## ✳️ Функции

- Логотип проекта и соответствующие данные отображаются в центре
- Предоставлять многоязычные функции и образцы шаблонов
- Обязательные инструкции README
- Встроенная функция навигации по каталогам для решения проблемы, заключающейся в том, что некоторые механизмы синтаксического анализа Markdown не могут правильно анализировать навигацию.

<span id="nav-6"></span>

## 🍊 Архитектура

<span id="nav-7"></span>

## 💎 Начиная

<span id="nav-8"></span>

## 🙆 Авторы

Автор этого проекта: [Misitebao](https://github.com/misitebao). старший инженер, предприниматель.

<span id="nav-9"></span>

## 🌟 Авторы

Спасибо всем участникам, которые участвовали в разработке template-git-repository. [Contributors](https://github.com/misitebao/template-git-repository/graphs/contributors)

<span id="nav-9-1"></span>

### 😵 Обмен сообществом

<span id="nav-10"></span>

## 👼 Часть пользователей

<span id="nav-11"></span>

## ☕ Жертвователи

<span id="nav-12"></span>

## 💰 Жертвователи

<span id="nav-13"></span>

## 👏 Спасибо

<span id="nav-14"></span>

## ©️ Лицензия

[License MIT](LICENSE)
