<p align="center">
  <img src="https://cdn.jsdelivr.net/gh/misitebao/CDN@master/gravatar_tigateam.png" width="40%" /><br/>
</p>
<p align="center">
开源Git存储库模板
</p>

<span id="nav-1"></span>

## 🌏 国际化

[English](README.md) | [简体中文](README.zh-Hans.md) | [русский](README.ru.md) | [français](README.fr.md)

<span id="nav-2"></span>

## 📚 内容目录

<details>
  <summary>点我 打开/关闭 目录列表</summary>

- [国际化](#nav-1)
- [内容目录](#nav-2)
- [项目介绍](#nav-3)
  - [官方网站](#nav-3-1)
- [图形演示](#nav-4)
- [功能特色](#nav-5)
- [架构](#nav-6)
- [新手入门](#nav-7)
- [关于作者](#nav-8)
- [贡献者](#nav-9)
  - [社区交流](#nav-9-1)
- [部分用户](#nav-10)
- [发布记录](CHANGE.md)
- [捐赠者](#nav-11)
- [赞助商](#nav-12)
- [特别鸣谢](#nav-13)
- [版权许可](#nav-14)

</details>

<span id="nav-3"></span>

## ℹ️ 项目介绍

此项目是一个 Github 示例仓库模板，主要内容为 README 的示例模板。

<span id="nav-3-1"></span>

### 🔔 官方网站

[如何写好开源项目的 README](https://blog.misitebao.com/posts/%E7%BC%96%E7%A8%8B%E6%8A%80%E6%9C%AF/%E5%A6%82%E4%BD%95%E5%86%99%E5%A5%BD%E5%BC%80%E6%BA%90%E9%A1%B9%E7%9B%AE%E7%9A%84readme-%E8%87%AA%E7%94%A8git%E4%BB%93%E5%BA%93%E6%A8%A1%E6%9D%BF%E5%88%86%E4%BA%AB/)
<span id="nav-4"></span>

## 🌅 图形演示

[![Click Gif to view the full demo](https://cdn.jsdelivr.net/gh/misitebao/CDN@main/md/template-git-repository-mini.gif)](https://www.bilibili.com/video/BV1d64y1B7pe?share_source=copy_web)

<span id="nav-5"></span>

## ✳️ 功能特色

- 项目 Logo 以及相应数据居中展示
- 提供多语言功能以及示例模板
- README 必备的说明
- 内置目录导航功能，解决部分 Markdown 解析引擎不能正确解析导航的问题

<span id="nav-6"></span>

## 🍊 架构

```
|—— .gitee                          Gitee 配置文件
| |—— ISSUE_TEMPLATE.md             Gitee Issue 模板
| |—— PULL_REQUEST_TEMPLATE.md      Gitee PR 模板
|—— .github                         Github 配置文件
| |—— ISSUE_TEMPLATE                Github Issue 模板
| | |—— issue-template-bug.md       Github Issue Bug 模板
| | |—— issue-template-feature.md   Github Issue Feature 模板
| |—— workflows                     Github 工作流
| | |—— deploy-for-hugo.yml         Github 工作流 Hugo 示例
| | |—— deploy-for-nodejs.yml       Github 工作流 NodeJS 示例
| |—— pull-request-template.md      Github PR 模板
|—— CHANGELOG.md                    发布日志
|—— LICENSE                         许可证
|—— README.md                       英语 README
|—— README.zh-Hans.md               其他语言 README
|—— README.tmpl.md                  README 模板

```

<span id="nav-7"></span>

## 💎 新手入门

[点我](README.zh-Hans.tmpl.md) 查看模板文件，再点击编辑即可复制模板内容。

<span id="nav-8"></span>

## 🙆 关于作者

本项目作者：[米司特包](https://github.com/misitebao)。高级工程师，创业者。

<span id="nav-9"></span>

## 🌟 贡献者

感谢所有参与 template-git-repository 开发的贡献者。[贡献者列表](https://github.com/misitebao/template-git-repository/graphs/contributors)

<span id="nav-9-1"></span>

### 😵 社区交流

<span id="nav-10"></span>

## 👼 部分用户

<span id="nav-11"></span>

## ☕ 捐赠者

<span id="nav-12"></span>

## 💰 赞助商

<span id="nav-13"></span>

## 👏 鸣谢

<span id="nav-14"></span>

## ©️ 版权许可

[License MIT](LICENSE)
