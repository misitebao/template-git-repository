<p align="center">
  <img src="https://cdn.jsdelivr.net/gh/misitebao/CDN@master/gravatar_tigateam.png" width="40%" /><br/>
</p>
<p align="center">
Open source Git repository template
</p>

<span id="nav-1"></span>

## 🌏 Internationalization

[English](README.md) | [简体中文](README.zh-Hans.md) | [русский](README.ru.md) | [français](README.fr.md)

<span id="nav-2"></span>

## 📚 Contents

- [Internationalization](#nav-1)
- [Contents](#nav-2)
- [Introductions](#nav-3)
  - [Official Website](#nav-3-1)
- [Graphic Demo](#nav-4)
- [Features](#nav-5)
- [Architecture](#nav-6)
- [Getting Started](#nav-7)
- [Authors](#nav-8)
- [Contributors](#nav-9)
  - [Community Exchange](#nav-9-1)
- [Part Of Users](#nav-10)
- [Release History](CHANGE.md)
- [Donators](#nav-11)
- [Sponsors](#nav-12)
- [Thanks](#nav-13)
- [License](#nav-14)

<span id="nav-3"></span>

## ℹ️ Introductions

This project is a Github sample warehouse template, the main content is the sample template of README.

<span id="nav-3-1"></span>

### 🔔 Official Website

[How to write a README for an open source project](https://blog.misitebao.com/posts/%E7%BC%96%E7%A8%8B%E6%8A%80%E6%9C%AF/%E5%A6%82%E4%BD%95%E5%86%99%E5%A5%BD%E5%BC%80%E6%BA%90%E9%A1%B9%E7%9B%AE%E7%9A%84readme-%E8%87%AA%E7%94%A8git%E4%BB%93%E5%BA%93%E6%A8%A1%E6%9D%BF%E5%88%86%E4%BA%AB/)

<span id="nav-4"></span>

## 🌅 Graphic Demo

![](https://cdn.jsdelivr.net/gh/misitebao/CDN@main/md/20210727130417.png)

<span id="nav-5"></span>

## ✳️ Features

- The project logo and corresponding data are displayed in the center
- Provide multi-language functions and sample templates
- README must-have instructions
- Built-in directory navigation function to solve the problem that some Markdown parsing engines cannot parse navigation correctly

<span id="nav-6"></span>

## 🍊 Architecture

<span id="nav-7"></span>

## 💎 Getting Started

<span id="nav-8"></span>

## 🙆 Authors

The author of this project: [Misitebao](https://github.com/misitebao). senior engineer, entrepreneur.

<span id="nav-9"></span>

## 🌟 Contributors

Thank you to all the contributors who participated in the development of template-git-repository. [Contributors](https://github.com/misitebao/template-git-repository/graphs/contributors)

<span id="nav-9-1"></span>

### 😵 Community Exchange

<span id="nav-10"></span>

## 👼 Part Of Users

<span id="nav-11"></span>

## ☕ Donators

<span id="nav-12"></span>

## 💰 Sponsors

<span id="nav-13"></span>

## 👏 Thanks

<span id="nav-14"></span>

## ©️ License

[License MIT](LICENSE)
